$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", async function(){

		const palabra = $('#busqueda').val();
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/

		try {

			//hago la petición, pero necesito la respuesta para obtener la información necesaria.
			const res = await fetch('http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=4ab35892c9b6ad97dca55f1d4149ce20&query='+palabra);
			
			console.log(res);

			if (res.status === 200) {
				const datos = await res.json();

				datos.results.forEach(peliculas => {
					miLista.append('<h5>'+peliculas.original_title+'</h5>');
					miLista.append('<p>'+peliculas.overview+'</p>');
					//console.log(peliculas.title);
					miLista.append('<img class="img-cardi" src="https://image.tmdb.org/t/p/w500/'+peliculas.poster_path+'" alt="Card image">');					
					miLista.append('<br/>');
				});
			}else if (res.status === 401) {
				console.log("Introduce los datos correctos");
			}
		} catch (error) {
			console.log(error);
		}
	});
});